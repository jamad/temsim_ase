DISTNAME = 'imreg'
DESCRIPTION = 'Image registration toolkit'
LONG_DESCRIPTION = """
"imreg" is an image registration package for python that makes it easy to
automatically align image data.
"""
MAINTAINER = 'Nathan Faggian, Riaan Van Den Dool, Stefan Van Der Walt'
MAINTAINER_EMAIL = 'nathan.faggian@gmail.com'
URL = 'pyimreg.github.com'
LICENSE = 'Apache License (2.0)'
DOWNLOAD_URL = ''
VERSION = '0.1'

from distutils.core import setup, Extension
import numpy


package_dir='temsim'

c_source=['autoslicmodule.cpp','autoslic.cpp','cfpix.cpp',
          'floatTIFF.cpp','slicelib.cpp']

c_source=[package_dir + '/' + x for x in c_source]



ext_modules=[Extension('autoslicmodule', c_source,include_dirs=[numpy.get_include()],
            extra_compile_args=['-fopenmp'],libraries=["fftw3f","fftw3f_threads"])]

setup(name='temsim',
      version='1.0',
      packages=['temsim'],
      ext_modules=ext_modules
      )

