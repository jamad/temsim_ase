ASE interface for TemSim
=============================

TemSim calculates high resolution (atomic or near atomic) conventional and 
scanning transmission electron microscope (TEM and STEM) images of thin specimens 
from first principles using the multislice method for electrons in the energy 
range of approximately 100 keV to 1000 keV.

The fundamental theory is described in "Advanced Computing in Electron Microscopy" 
(Plenum 1998, Springer 2010) by Earl J. Kirkland. If you find the programs useful 
please cite the book. 

This project interfaces the TemSim code with the Atomic Simulation Environment (ASE).

TemSim webpage: http://people.ccmr.cornell.edu/~kirkland/

ASE webpage: http://wiki.fysik.dtu.dk/ase

Requirements
------------

* ASE_
* Python_
* NumPy_
* FFTW_

Installation
------------
Install FFTW configured as specified below (see `doc <http://www.fftw.org/doc/
Installation-on-Unix.html>`_)::

    $ ./configure --enable-float --enable-threads --enable-shared
    $ make
    $ make install

Make sure that the FFTW libraries are installed in a directory on your $LIBRARY_PATH.
The FFTW installation directory can be modified with a "--prefix" flag.

To compile and install TemSim with the ASE interface, use::

    $ CFLAGS="-fopenmp" python setup.py install

Example
------------
Set up graphite and make a basic simulated TEM image (image included in /examples):

>>> from temsim.tem import TEM
>>> from ase.lattice.hexagonal import Graphite
>>> a,c=2.46,6.70 # lattice constants
>>> directions=[[1,-2,1,0],[2,0,-2,0],[0,0,0,1]] # a right-angled unit cell
>>> atoms = Graphite(symbol='C', latticeconstant={'a':a,'c':c},
                    directions=directions, size=(10,5,5))
>>> TEM(atoms,fileout='graphite.tif',
        energy=300,               # acceleration energy in keV
        defocus=-30,              # defocus in Angstrom
        aberrations={'C30':1.3})  # spherical aberration in mm

.. _ASE: http://wiki.fysik.dtu.dk/ase
.. _Python: http://www.python.org/
.. _NumPy: http://docs.scipy.org/doc/numpy/reference/
.. _FFTW: http://www.fftw.org/