from temsim.tem import TEM
from ase.lattice.hexagonal import Graphite

a,c=2.46,6.70 # lattice constants

# TemSim assumes a right-angled unit cell
directions=[[1,-2,1,0],[2,0,-2,0],[0,0,0,1]]

atoms = Graphite(symbol='C', latticeconstant={'a':a,'c':c},
                 directions=directions, size=(10,5,5))

TEM(atoms,fileout='graphite.tif',
    energy=300,               # acceleration energy in keV
    defocus=-30,              # defocus in Angstrom
    aberrations={'C30':1.3})  # spherical aberration in mm
