import autoslicmodule
import numpy as np
import ctypes
import time

def TEM(atoms,fileout,partial_coherence=True,img_size=256,delta_z=2.,energy=300.,defocus=-30.,
        tilt=(0.,0.),aberrations={},temperature=0.,num_displacements=30,seed=None,
        vibration_std=None,occupation=None,illum_angles=(0.,0.),focal_spread=0.,focal_sampling=0.1,
        obj_aperture=35.,wave_in=None):
    """
    Simulate a transmission electron microscopy image from an Atoms object

    Parameters:
    
    atoms: Atoms object
        The Atoms object to simulate a TEM image of
    fileout: str
        Name of output image file to write simulation result
    partial_coherence: bool
        Include partial coherence?
    img_size: one or two int
        Size of output image in pixels
    delta_z: float
        Slice thickness in the multislice algorithm (in Angstrom)
    energy: float
        Electron energy (acceleration voltage) (in keV)
    defocus: float or str
        Defocus (mean value) of the microscope (in mm)
    tilt: two float
        Tilt of the system in the x and y direction (in mrad)
    abberations: dict of floats
        Dictionary of microscope abberations up to fifth order (in mm). The allowed keys:
            C12a, C12b, C21a, C21b, C23a, C23b, C30, C32a, C32b, C34a, C34b, C41a, 
            C41b, C43a, C43b, C45a, C45b, C50, C52a, C52b, C54a, C54b, C56a, C56b
    temperature: float
        Temperature (in degrees K)
    num_displacements: int
        Number of configurations used in thermal averaging ensemble
    seed: int
        Seed for random number generator used for generating thermal averaging ensemble.
        If set to None a seed number is generated from the current date and time
    vibration_std: list of float
        List of standard deviations of the vibration amplitude - one for each atom
        The vibration amplitude is scaled by temperature as sqrt(temperature/300)
    occupation: list of float
        List occupation numbers - one for each atom    
    illum_angles: two float
        Minimum and maximum illumination angle (in mrad)
    focal_spread: float
        Focal spread (in Angstrom)
    focal_sampling: float
        Sampling size of focal spread (in Angstrom)
    obj_aperture: float
        Objective len aperture (in mrad)
    wave_in: str
        Name of complex image file with the initial wave function
    """
    
    pos=atoms.get_positions()    
    Znum=atoms.get_atomic_numbers()
    
    x=(ctypes.c_float * len(pos[:,0]))(*pos[:,0])
    y=(ctypes.c_float * len(pos[:,1]))(*pos[:,1])
    z=(ctypes.c_float * len(pos[:,2]))(*pos[:,2])
    Znum=(ctypes.c_int * len(Znum))(*Znum)

    cell=atoms.get_cell()
    if any(cell[0,1:3]>1e-10)|any(cell[1,[0,2]]>1e-10)|any(cell[2,0:2]>1e-10):
        raise Warning("Temsim assumes right-angled unit cell - results may be unexpected")

    if isinstance(img_size,int):
        img_size=(img_size,img_size)

    if delta_z < 1.:
        raise Warning("Slice thickness is probably too thin")
    
    aber_symb = ["C12a", "C12b", "C21a", "C21b", "C23a", "C23b", "C30", "C32a", "C32b", 
                 "C34a", "C34b", "C41a", "C41b", "C43a", "C43b", "C45a", "C45b", "C50", 
                 "C52a", "C52b", "C54a", "C54b", "C56a", "C56b"]    
    unrec_aber = set(aberrations)-set(aber_symb)
    if unrec_aber:
        raise ValueError("Unrecognized aberration(s) {0}".format(", ".join(list(unrec_aber))))

    aber = dict(zip(aber_symb, [0.]*len(aber_symb)))
    aber.update(aberrations)
    aber_list = np.array([aber[s] for s in aber_symb])
    aber_list=(ctypes.c_float * len(aber_list))(*aber_list)
    
    if any(aber_list[0:6])|any(aber_list[7:17])|any(aber_list[18:-1]):
        multiMode=True
    else:
        multiMode=False
    
    if temperature <= 0.:
        lwobble=False
        num_displacements=1
    else:
        lwobble=True

    if seed is None:
        seed=int(time.time())

    if occupation is None:
        occupation=np.array([1.]*len(atoms))
    occ=(ctypes.c_float * len(occupation))(*occupation)

    assert len(occupation)==len(atoms)

    if (vibration_std is None)&lwobble:
        raise Warning("Using non-zero temperature without setting the" +
                      "vibration standard deviation - using default values")
        vibration_std = [.1]*len(atoms)
    elif vibration_std is None:
        vibration_std = [0.]*len(atoms)
    
    wobble=(ctypes.c_float * len(vibration_std))(*vibration_std)

    assert len(wobble)==len(atoms)    

    if wave_in is not None:
        if partial_coherence:
            raise ValueError("Input wavefunction can not be used with partial coherence")
        else:
            use_wave_in=True
    else:
        use_wave_in=False
        wave_in="-"
    
    autoslicmodule.tem(x=x, y=y, z=z, Znum=Znum, natom=len(atoms), ax=cell[0,0], by=cell[1,1], 
                       fileout=fileout, lpartl=partial_coherence, nx=img_size[0], ny=img_size[1], 
                       deltaz=delta_z, v0=energy, df0=defocus, ctiltx=tilt[0], ctilty=tilt[1], 
                       obj_aber=aber_list, multiMode=multiMode, temperature=temperature, 
                       nwobble=num_displacements, lwobble=lwobble, iseed=seed, wobble=wobble, 
                       occ=occ, acmin=illum_angles[0], acmax=illum_angles[1], sigmaf=focal_spread, 
                       dfdelt=focal_sampling, aobj=obj_aperture, filestart=wave_in, 
                       lstart=use_wave_in)
