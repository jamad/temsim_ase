#include <Python.h>
#include <numpy/ndarraytypes.h>
#include <numpy/arrayobject.h>

#include <cstdio>  /* ANSI C libraries */
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <ctime>

#include <string>
#include <iostream>  //  C++ stream IO
#include <fstream>
#include <iomanip>   //  to format the output

using namespace std;

#include "cfpix.hpp"       // complex image handler with FFT
#include "slicelib.hpp"    // misc. routines for multislice
#include "floatTIFF.hpp"   // file I/O routines in TIFF format
#include "autoslic.hpp"    // the calculation engine

#ifdef USE_OPENMP
#include <omp.h>
/*  get wall time for benchmarking openMP */
#define walltim() ( omp_get_wtime() )
double walltimer;
#endif

static PyObject* tem(PyObject* self, PyObject* args, PyObject* kw)
{

  string filein, filestart, filebeam, filecross;
  
  const char version[] = "21-feb-2015 (ejk)";
  
  const char *fileout;

  int lstart=0, lpartl=0, lbeams=0, lwobble=0, lcross=0, nwobble=1;
  int ix, iy, iz, nx, ny, nzout, i, nslic0, islice,
    ndf, nbout, ib, ncellx, ncelly, ncellz, NPARAM;
  int nillum, nzbeams;
  int *hbeam, *kbeam;
  int natom, *Znum, done, status, multiMode;
  long  ltime;
  
  unsigned long iseed;
  
  float v0, mm0, wavlen, rx, ry, ax, by, cz, pi,
    rmin, rmax, aimin, aimax, ctiltx, ctilty,
    acmin, acmax, df0, sigmaf, dfdelt, aobj,
    temperature, ycross, dx, dy;
  
  float wmin, wmax, xmin,xmax, ymin, ymax, zmin, zmax;
  
  float *x, *y, *z, *occ, *wobble;
  float *param, *sparam, *aber;
  
  double timer, deltaz, vz;
  
  ofstream fp1;
  
  cfpix pix;		// to get results of calculation
  cfpix wave0;	        // initial wavefunction (if used)
  cfpix depthpix;	// to get xz cross section results
  cfpix beams;	        // to get valuse of requested beams during propagation
  
  floatTIFF myFile;	//  file input/output
  autoslic aslice;	// has the calculation engine

  PyArrayObject* obj_x = NULL;
  PyArrayObject* obj_y = NULL;
  PyArrayObject* obj_z = NULL;
  PyArrayObject* obj_Znum = NULL;
  PyArrayObject* obj_occ = NULL;
  PyArrayObject* obj_wobble = NULL;
  PyArrayObject* obj_aber = NULL;
 
  static char *argnames[] = {"x","y","z","Znum","natom","ax","by","fileout","lpartl",
			     "nx","ny","deltaz","v0","df0","ctiltx","ctilty","obj_aber",
			     "multiMode","temperature","nwobble","lwobble","iseed","wobble",
			     "occ","acmin","acmax","sigmaf","dfdelt","aobj","filestart",
			     "lstart",NULL};
  
  if (!PyArg_ParseTupleAndKeywords(args, kw, "OOOOiffsiiidffffOifiiiOO|fffffsi", argnames, 
				   &obj_x, &obj_y, &obj_z, &obj_Znum, &natom, &ax, &by, &fileout, &lpartl, 
				   &nx, &ny, &deltaz, &v0, &df0, &ctiltx, &ctilty, &obj_aber, 
				   &multiMode, &temperature, &nwobble, &lwobble, &iseed, &obj_wobble,
				   &obj_occ, &acmin, &acmax, &sigmaf, &dfdelt, &aobj, &filestart,
				   &lstart))
    return NULL;
  
  x = (float*) PyArray_DATA(obj_x);
  y = (float*) PyArray_DATA(obj_y);
  z = (float*) PyArray_DATA(obj_z);
  occ = (float*) PyArray_DATA(obj_occ);
  wobble = (float*) PyArray_DATA(obj_wobble);
  Znum = (int*) PyArray_DATA(obj_Znum);
  aber = (float*) PyArray_DATA(obj_aber);

  acmin = acmin * 0.001F;
  acmax = acmax * 0.001F;
  aobj = (float) fabs(aobj * 0.001F);
  ctiltx = ctiltx / 1000;
  ctilty = ctilty / 1000;
  
  mm0 = 1.0F + v0/511.0F;
  wavlen = (float) wavelength( v0 );

  /*  calculate the total specimen volume and echo */
  xmin = xmax = x[0];
  ymin = ymax = y[0];
  zmin = zmax = z[0];
  wmin = wmax = wobble[0];
  
  for( i=0; i<natom; i++) {
    if( x[i] < xmin ) xmin = x[i];
    if( x[i] > xmax ) xmax = x[i];
    if( y[i] < ymin ) ymin = y[i];
    if( y[i] > ymax ) ymax = y[i];
    if( z[i] < zmin ) zmin = z[i];
    if( z[i] > zmax ) zmax = z[i];
    if( wobble[i] < wmin ) wmin = wobble[i];
    if( wobble[i] > wmax ) wmax = wobble[i];
  }
  


  
  //   set calculation flags
  aslice.lbeams = lbeams; // currently not implemented
  aslice.lcross = lcross; // currently not implemented
  aslice.lpartl = lpartl;
  aslice.lstart = lstart;
  aslice.lwobble = lwobble;
  
  //   set calculation parameters (some already set above)
  NPARAM = myFile.maxParam();
  param = (float*) malloc1D( NPARAM, sizeof(float), "param" );
  sparam = (float*) malloc1D( NPARAM, sizeof(float), "sparam" );
  for( ix=0; ix<NPARAM; ix++ ) param[ix] = 0.0F;
  
  int aberIndex[]={pC12a, pC12b, pC21a, pC21b, pC23a, pC23b, pCS,
		   pC32a, pC32b, pC34a, pC34b, pC41a, pC41b, pC43a, pC43b, pC45a, pC45b, 
		   pCS5, pC52a, pC52b, pC54a, pC54b, pC56a, pC56b };

  int Naber=24;
  for(i=0; i<Naber; i++) {
    param[aberIndex[i]] = (float) (aber[i] * 1.0e7);   /* mm. to Ang. */
  }
  
  
  param[pDEFOCUS] = (float) df0;
  param[pDDF] = (float) sigmaf;
  param[ pAX ] = ax;			// supercell size
  param[ pBY ] = by;
  param[ pNX ] = (float) nx;
  param[ pNY ] = (float) ny;
  param[pENERGY]   =  v0;
  param[ pDELTAZ ] = (float) deltaz;	// slice thickness
  param[ pOAPERT ] = aobj;
  param[ pXCTILT ] = ctiltx;		// crystal tilt
  param[ pYCTILT ] = ctilty;
  param[pCAPERT] = acmax;		// condencer angles
  param[pCAPERTMIN] = acmin;
  param[ pTEMPER ] = (float) fabs( temperature );
  param[ pNWOBBLE ] = (float) nwobble;	//  number config. to average
  param[pWAVEL] = wavlen;			//  probably recal. autoslice::calculate()
  
  param[pMODE] = 6;  // save mode = autoslic
  
  if ( lpartl == 1 ) {
    param[pDEFOCUS] = df0;
    param[pOAPERT] = aobj;
    param[pDDF] = sigmaf;
    param[pCAPERT] = acmax;
  }

  timer = cputim();
#ifdef USE_OPENMP
  walltimer = walltim();  /* wall time for openMP */
#endif
  
  cout << "autoslic(e) version dated " << version << endl;
  cout << "Copyright (C) 1998-2014 Earl J. Kirkland"  << endl;
  cout << "This program is provided AS-IS with ABSOLUTELY NO WARRANTY"  << endl;
  cout <<     " under the GNU general public license" << endl << endl;
  
  cout << "perform CTEM multislice with automatic slicing and FFTW" << endl;
#ifdef USE_OPENMP
  cout << "and multithreaded using openMP" << endl;
#endif
  cout <<  " "  << endl;

/*  get starting value of transmitted wavefunction if required
   (this can only be used in coherent mode)
    remember to save params for final output pix  */

  if ( lstart == 1 ) {
    if( myFile.read( filestart.c_str() ) != 1 ) {
      cout << "Cannot open input file: " << filestart << endl; 
      exit( 0 );
    }
    
    if( myFile.getnpix() != 2 ) {
      cout << "Input file " << filestart << " must be complex, can't continue." << endl;
      exit( 0 );
    }
    
    nx =  myFile.nx();
    ny =  myFile.ny();
    
    nx = nx/2;
    wave0.resize( nx, ny );
    
    //  save starting pix for later
    for( ix=0; ix<nx; ix++) for( iy=0; iy<ny; iy++) {
	wave0.re(ix,iy) = myFile(ix,iy);
	wave0.im(ix,iy) = myFile(ix+nx,iy);
      }
    
    //  save parameters to verify successive images are same size etc.
    for( i=0; i<NPARAM; i++) sparam[i] = myFile.getParam( i );
    
    ax = sparam[pDX] * nx;
    by = sparam[pDY] * ny;
    v0     = sparam[pENERGY];
    nslic0 = (int) sparam[pNSLICES];
    cout << "Starting pix range " << sparam[pRMIN] << " to " << sparam[pRMAX] 
	 << " real\n" << "           " << sparam[pIMIN] << " to " 
	 << sparam[pIMAX] << " imag" << endl;
    cout << "Beam voltage = " << v0 << " kV" << endl;
    cout << "Old crystal tilt x,y = " << 1000.*sparam[pXCTILT] <<  ", " 
	 << 1000.*sparam[pYCTILT] << " mrad" << endl;
    
  } else nslic0 = 0;     /* end if( lstart...) */

  cout << "Random number seed initialized to " << iseed << endl;
  cout << "electron wavelength = " << wavlen << " Angstroms" << endl;
  cout << natom << " atomic coordinates read in"  << endl;
  cout <<  " "  << endl;

  cout <<"Size in pixels Nx, Ny= " << nx << " x " << ny << " = " << nx*ny 
       << " beams" << endl;
  cout <<"Lattice constant a,b = " << ax << ", " << by << endl;

  cout << "Total specimen range is\n" 
       << xmin << " to " << xmax << " in x\n"
       << ymin << " to " << ymax << " in y\n"
       << zmin << " to " << zmax << " in z" << endl;
  
  if( lwobble == 1 )
    cout << "Range of thermal rms displacements (300K) = "
	 << wmin << " to " << wmax << endl;

  cout << "Illumination angle sampling (in mrad) = "
       << 1000.*1.0F/ax*wavlen << ", " << 1000.*1.0F/by*wavlen << "\n" << endl;
  
  
  // ------- iterate the multislice algorithm proper -----------

  aslice.calculate( pix, wave0, depthpix, param, multiMode, natom, &iseed,
  		    Znum, x, y, z, occ, wobble, beams, hbeam, kbeam, nbout, ycross, dfdelt );

  if( lpartl == 1 ) {         //    with partial coherence
    nillum = aslice.nillum;
    cout << "Total number of illumination angle = "
	 << nillum << endl;
    ndf = (int) ( ( 2.5F * sigmaf ) / dfdelt );  // recal same value in class
    cout << "Total number of defocus values = " << 2*ndf+1 << endl;
  }
  
  else if( lbeams ==1 ) {
    fp1.open( filebeam.c_str() );
    if( fp1.bad() ) {
      cout << "can't open file " << filebeam << endl;
      exit(0);
    }
    fp1 << " (h,k) = " ;
    for(ib=0; ib<nbout; ib++)
      fp1 << " (" << hbeam[ib] << "," << kbeam[ib] << ")";
    fp1 << endl;
    nzbeams = beams.ny();
    fp1 << "nslice, (real,imag) (real,imag) ...\n" << endl;
    for( islice=0; islice<nzbeams; islice++) {
      fp1 << setw(5) << islice+1;   //  remember it starts after first slice
      for( ib=0; ib<nbout; ib++)  //  setprecision(4)
	fp1 << "  " << setw(10) << beams.re(ib,islice)
	    << "  " << setw(10) << beams.im(ib,islice); 
      fp1 << endl;
    }
    fp1.close();
    
  } // end else 
  
  /*  ------------------------------------------------------
      output results and find min and max to echo
      remember that complex pix are stored in the file in FORTRAN
      order for compatibility */
  
  pix.findRange( rmin, rmax, aimin, aimax );
  
  param[pRMAX]  = rmax;
  param[pIMAX]  = aimax;
  param[pRMIN]  = rmin;
  param[pIMIN]  = aimin;
  
  param[pDX] = dx = (float) ( ax/((float)nx) );
  param[pDY] = dy = (float) ( by/((float)ny) );
  
  //param[pNSLICES] = 0.0F;  /* ??? */
  
  for( ix=0; ix<NPARAM; ix++ ) myFile.setParam( ix, param[ix] );
  
  if ( lpartl == 1 ) {
    myFile.resize( nx, ny );
    myFile.setnpix( 1 );
    for( ix=0; ix<nx; ix++) 
      for( iy=0; iy<ny; iy++)
	myFile(ix,iy) = pix.re(ix,iy);
  } else {
    myFile.resize( 2*nx, ny );
    myFile.setnpix( 2 );
    for( ix=0; ix<nx; ix++) for( iy=0; iy<ny; iy++) {
	myFile(ix,iy)    = pix.re(ix,iy);
	myFile(ix+nx,iy) = pix.im(ix,iy);
      }
  }
  
  i = myFile.write( fileout, rmin, rmax, aimin, aimax, dx, dy );
  if( i != 1 ) cout << "autoslice cannot write TIF file " << fileout << endl;
  cout << "pix range " << rmin << " to " << rmax << " real,\n" <<
    "          " << aimin << " to " << aimax << " imag" << endl;
  
  /* ----- output depth cross section if requested ------- */
  if( lcross == 1 ){
    depthpix.findRange( rmin, rmax, aimin, aimax );
    myFile.setParam( pRMAX, rmax );
    myFile.setParam( pIMAX, 0.0F );
    myFile.setParam( pRMIN, rmin );
    myFile.setParam( pIMIN, 0.0F );
    myFile.setParam( pDY, dy = (float) ( deltaz ) );
    
    nzout = depthpix.ny();
    myFile.resize( nx, nzout );
    myFile.setnpix( 1 );
    for( ix=0; ix<nx; ix++) for( iz=0; iz<nzout; iz++) {
	myFile(ix,iz) = depthpix.re(ix,iz);
      }
    i = myFile.write( filecross.c_str(), rmin, rmax, aimin, aimax, dx, dy );
    
    if( i != 1 ) cout << "autoslice cannot write TIF file "
		      << filecross << endl;
    cout << "depth pix range " << rmin << " to " << rmax << " real" << endl;
  }
  
  cout << "Total CPU time = " << cputim()-timer << " sec." << endl;
#ifdef USE_OPENMP
  cout << "wall time = " << walltim()-walltimer << " sec." << endl;
#endif
  
  return Py_BuildValue("i", 0);  
}

static PyMethodDef autoslicModuleMethods[] =
{
	{"tem", (PyCFunction)tem, METH_VARARGS | METH_KEYWORDS, "run TEM simulation"},
	{NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initautoslicmodule(void)
{
	(void) Py_InitModule("autoslicmodule", autoslicModuleMethods);
	import_array();
}

